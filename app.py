from flask import Flask,request,render_template,redirect,flash,session
from flask_mail import Mail,Message
import requests
import sys
import json
import os
app = Flask(__name__)

mail_settings = {
    "MAIL_SERVER": 'smtp.gmail.com',
    "MAIL_PORT": 465,
    "MAIL_USE_TLS": False,
    "MAIL_USE_SSL": True,
    "MAIL_USERNAME": 'woodenworkout@gmail.com',
    "MAIL_PASSWORD": 'Goyescos'
}
app.config.update(mail_settings)
mail=Mail(app)

@app.route('/')
def home():
	return render_template('index.html')

@app.route('/progresiones')
def progresiones():
	progresiones=get_progressions()
	total_ids=get_total_progressions()
	i=0
	for progresion in progresiones:
		progresion.insert(0, total_ids[i])
		# progresion.append(total_ids[i])
		i=i+1
		#print(progresion[0])
	return render_template('progresiones.html',progresiones=progresiones)

@app.route('/entrenamientos')
def entrenamientos():
    entrenos=get_trains()
    total_ids=get_total_trains()

    principiante=[]
    intermedio=[]
    intermedio_plus=[]

    i=0
    for entreno in entrenos:
        entreno.append(total_ids[i])
        i=i+1
        if entreno[3]=="Principiante\n":
            principiante.append(entreno)        
        elif entreno[3]=="Intermedio\n":    
            intermedio.append(entreno)
        else:    
            intermedio_plus.append(entreno)
    print("Tamaño principiante: "+ str(len(principiante))+"Tamaño intermedio: "+ str(len(intermedio)))
    return render_template('entrenamientos.html',entrenos=entrenos, principiante=principiante,intermedio=intermedio,intermedio_plus=intermedio_plus)

@app.route('/contacto')
def contacto():
	return render_template('contact.html')	

# @app.route('/single_progresion')
# def single_progresion():
# 	entrenos=get_trains()
# 	entreno=entrenos[0]
# 	return render_template('portfolio-single.html',entreno=entreno)

@app.route('/progresion_prueba/<idProgression>')
def progresion_prueba(idProgression):
	progresiones=get_progressions()
	progresion=progresiones[int(idProgression)]
	total_steps=progresion[1]
	total_steps=int(total_steps)

	return render_template('progresion-single.html',progresion=progresion,total_steps=total_steps)

@app.route('/single_train/<idTrain>', methods=['GET','POST'])
def single_train(idTrain):
	entrenos=get_trains()
	entreno=entrenos[int(idTrain)]
	return render_template('portfolio-single.html',entreno=entreno)	

@app.route('/show_login')
def show_login():
	return render_template('log_page.html')

@app.route('/anyadir_progresion')
def anyadir_progresion():
	return render_template('progresion_form.html')

@app.route('/anyadir_entrenamiento')
def anyadir_entrenamiento():
	return render_template('entrenamiento_form.html')	

@app.route('/coger_info_entreno',methods=['POST'])
def coger_info_entreno():
    args=[]
    args.append(request.form["nombre"])
    args.append(request.form["fecha"])
    args.append(request.form["creador"])
    args.append(request.form["nivel"])
    args.append(request.form["brief"])
    args.append(request.form["groups"])
    args.append(request.form["objective"])
    args.append(request.form["Tipo"])
    args.append(request.form["text"])
    store_train(args)
    #print(args)
    return redirect('seccion_admin')

    
@app.route('/coger_info_progresion',methods=['POST'])
def coger_info_progresion():
	args=[]
	img=request.files["img"]
	name=request.form["nombre"]
	name_file=name+".jpeg"
	img.save(os.path.join('static', name_file))
	args.append(name)
	args.append(request.form["e1"])
	args.append(request.form["d1"])
	args.append(request.form["e2"])
	args.append(request.form["d2"])
	args.append(request.form["e3"])
	args.append(request.form["d3"])
	args.append(request.form["e4"])
	args.append(request.form["d4"])
	e5=request.form["e5"]
	d5=request.form["d5"]
	total_steps=4
	if e5 != "":
		args.append(request.form["e5"])
		args.append(request.form["d5"])
		total_steps=total_steps+1
	store_progresion(args,total_steps)
	return redirect('seccion_admin')

@app.route('/login',methods=['POST'])
def login():
    username=request.form['username1']
    password=request.form['password1']
    error="Zona únicamente para administración"
    if username=='admin' and password=='Goyescos':
        return redirect('seccion_admin')
    else:    
        return render_template('index.html',error=error)


@app.route('/seccion_admin')
def seccion_admin():
    return render_template('index_admins.html')

@app.route('/sitemap')
def sitemap():
    return render_template('sitemap.xml')

@app.route('/get_mail',methods=['POST'])
def get_mail():
    nombre=request.form['nombre']
    apellido=request.form['apellido']
    email=request.form['email']
    texto=request.form['texto']
    nuestro_email=["moncayoworkout@gmail.com"]
    body="Nos contacta "+nombre+" "+apellido+".\n"+"Su email es: "+email+".\n"+texto
    msg = Message(subject="Mensaje de contacto",
                      sender=app.config.get("MAIL_USERNAME"),
                      recipients=nuestro_email, # replace with your email for testing
                      body=body)
    mail.send(msg)
    return render_template('index.html')



#------------------------------------
#------------------------------------

#Added functions for functionality of the server

#------------------------------------
#------------------------------------

def store_train(args):
    f=open("static/trains.txt", "a")
    f.write("-"+args[0]+"\n")
    for i in range(8):
        f.write(args[i+1]+"\n")
    
    f.write("%\n")
    f.close()
    f=open("static/trains_number.txt", "r")
    total=int(f.readline())
    f.close()
    total=total+1
    f=open("static/trains_number.txt", "w")
    f.write(str(total))
    f.close()

def get_trains():
    lista_entrenamientos=[]
    end=False
    f=open("static/trains.txt", "r")
    while not end:
        entrenamiento=[]
        l=f.readline(1)
        if l=="":
        	break
        for x in range(8):
       	    entrenamiento.append(f.readline())
       	whole_text=""    
       	for x in range(30):
       	    l1=f.readline()
       	    if l1 == "%\n":
       	        break
            else:
                whole_text=whole_text+l1
        entrenamiento.append(whole_text)
        lista_entrenamientos.append(entrenamiento)    	
    return lista_entrenamientos        



def get_total_trains():
	f=open("static/trains_number.txt","r")
	total=f.readline()
	ids=[]
	total=int(total)
	for i in range(total):
		ids.append(str(i))
	return ids


def store_progresion(args,total_steps):
    f=open("static/progressions.txt", "a")
    f.write("-"+args[0]+"\n")
    f.write(str(total_steps)+"\n")
    for i in range(1,total_steps*2+1,2):
        embed_url=get_embed(args[i])
        f.write(embed_url+"\n")
        f.write(args[i+1]+"\n")
    
    f.write("%\n")
    f.close()
    f=open("static/progressions_number.txt", "r")
    total=int(f.readline())
    f.close()
    total=total+1
    f=open("static/progressions_number.txt", "w")
    f.write(str(total))
    f.close()

def get_progressions():
    lista_progresiones=[]
    end=False
    f=open("static/progressions.txt", "r")
    while not end:
        progresion=[]
        l=f.readline(1)
        if l=="":
        	break
        name=f.readline()
        total_steps=f.readline()
        progresion.append(name)
        progresion.append(total_steps)
        for x in range(int(total_steps)):
       	    progresion.append(f.readline())
       	    progresion.append(f.readline())
       	# whole_text=""    
       	# for x in range(30):
       	#     l1=f.readline()
       	#     if l1 == "%\n":
       	#         break
        #     else:
        #         whole_text=whole_text+l1
        # entrenamiento.append(whole_text)
        f.readline()
        lista_progresiones.append(progresion)    	
    return lista_progresiones

def get_total_progressions():
	f=open("static/progressions_number.txt","r")
	total=f.readline()
	ids=[]
	total=int(total)
	for i in range(total):
		ids.append(str(i))
	return ids    

def get_embed(url):
	if (url.find("watch")!= -1):
		first_split=url.split("v=")
		#print("ESTO HE SACADO")
		#print(first_split[0])
		second_split=first_split[1].split("&")
		videoId=second_split[0]
	else:
		second_split=url.split("/")	
		videoId=second_split[3]
	prefix="https://www.youtube.com/embed/"
	autoplay="?autoplay=0"
	final_embed=prefix+videoId+autoplay
	return final_embed


if __name__=='__main__':
    if sys.argv[1]=="local":
	    app.run(debug=True,host='127.0.0.1',port='5000')
    
    else:
        app.run(debug=False,host='192.168.1.54',port='80')		
